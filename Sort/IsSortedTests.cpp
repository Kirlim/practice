#include"../catch.hpp"

#include<vector>
#include"IsSorted.h"



namespace Sort
{
	TEST_CASE("An empty vector is sorted", "[IsSorted]")
	{
		std::vector<int> vector{};
		REQUIRE(IsSorted(std::begin(vector), std::end(vector)));
	}

	TEST_CASE("A vector with one element is sorted", "[IsSorted]")
	{
		std::vector<int> vector = { 1 };
		REQUIRE(IsSorted(std::begin(vector), std::end(vector)));
	}

	TEST_CASE("Sorted examples", "[IsSorted]")
	{
		std::vector<int> vector1 = { 10, 20 };
		REQUIRE(IsSorted(std::begin(vector1), std::end(vector1)));

		std::vector<int> vector2 = { 1, 3, 4 };
		REQUIRE(IsSorted(std::begin(vector2), std::end(vector2)));

		std::vector<int> vector3 = { -1, 2, 3, 19, 159 };
		REQUIRE(IsSorted(std::begin(vector3), std::end(vector3)));
	}

	TEST_CASE("Not sorted examples", "[IsSorted]")
	{
		std::vector<int> vector1 = { 20, 10 };
		REQUIRE_FALSE(IsSorted(std::begin(vector1), std::end(vector1)));

		std::vector<int> vector2 = { 1, 3, 2 };
		REQUIRE_FALSE(IsSorted(std::begin(vector2), std::end(vector2)));

		std::vector<int> vector3 = { 1, 2, 3, -19, 159 };
		REQUIRE_FALSE(IsSorted(std::begin(vector3), std::end(vector3)));
	}
}