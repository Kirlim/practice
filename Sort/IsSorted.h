#pragma once

#include<vector>



namespace Sort
{
	template <typename It>
	bool IsSorted(const It begin, const It end)
	{
		auto previous = begin;
		auto next = begin;

		while (next != end)
		{
			if (*previous > *next)
				return false;

			previous = next;
			next++;
		}

		return true;
	}
}