#include"../catch.hpp"

#include<vector>
#include"BubbleSort.h"
#include"IsSorted.h"



namespace Sort::BubbleSort
{
	TEST_CASE("Sorting empty vector results in empty vector", "[BubbleSort]")
	{
		std::vector<int> vector{};

		SECTION("Common Bubble Sort")
		{
			Sort(std::begin(vector), std::end(vector));
			REQUIRE(vector.size() == 0);
		}

		SECTION("Optimized Bubble Sort")
		{
			OptimizedSort(std::begin(vector), std::end(vector));
			REQUIRE(vector.size() == 0);
		}
	}

	TEST_CASE("Sorting vector with one element results in the same vector", "[BubbleSort]")
	{
		std::vector<int> vector{};
		vector.push_back(10);

		SECTION("Common Bubble Sort")
		{
			Sort(std::begin(vector), std::end(vector));

			REQUIRE(vector.size() == 1);
			REQUIRE(vector[0] == 10);
		}

		SECTION("Optimized Bubble Sort")
		{
			OptimizedSort(std::begin(vector), std::end(vector));

			REQUIRE(vector.size() == 1);
			REQUIRE(vector[0] == 10);
		}
	}

	TEST_CASE("Sorting vector with only two elements", "[BubbleSort]")
	{
		std::vector<int> vector = { 5, -3 };

		SECTION("Common Bubble Sort")
		{
			Sort(std::begin(vector), std::end(vector));

			REQUIRE(vector.size() == 2);
			REQUIRE(IsSorted(std::begin(vector), std::end(vector)));
		}

		SECTION("Optimized Bubble Sort")
		{
			OptimizedSort(std::begin(vector), std::end(vector));

			REQUIRE(vector.size() == 2);
			REQUIRE(IsSorted(std::begin(vector), std::end(vector)));
		}
	}

	TEST_CASE("Sorting vector", "[BubbleSort]")
	{
		std::vector<int> vector = { 10,9,8,7,6,5,4,3,2,1,0,-1,-2,-3,-4,-5,-6,-7,-8,-9,-10 };

		SECTION("Common Bubble Sort")
		{
			Sort(std::begin(vector), std::end(vector));

			REQUIRE(vector.size() == 21);
			REQUIRE(IsSorted(std::begin(vector), std::end(vector)));
		}

		SECTION("Optimized Bubble Sort")
		{
			OptimizedSort(std::begin(vector), std::end(vector));

			REQUIRE(vector.size() == 21);
			REQUIRE(IsSorted(std::begin(vector), std::end(vector)));
		}
	}
}