#pragma once

#include<vector>



// Bubble Sort algorithms are implemented by using Random Access Iterators
// The sorted element type must be swappable as per std::swap.
namespace Sort::BubbleSort
{
	// TODO : rewrite in terms of iterators only
	template <typename RanAccIt>
	void Sort(const RanAccIt begin, const RanAccIt end)
	{
		for (int i = 0; i < (end - begin); ++i)
		{
			// swapped allows to early quit if the vector is already sorted.
			// Since the complexity of the Bubble Sort is O(n^2), early quitting
			// is a good guess if the vector is already sorted
			bool swapped = false;

			auto previous = begin;
			for (auto next = (begin + 1); next != end; next++)
			{
				if (*previous > *next)
				{
					std::swap(*previous, *next);
					swapped = true;
				}

				previous = next;
			}

			if (!swapped)
				return;
		}
	}

	template <typename RanAccIt>
	void OptimizedSort(const RanAccIt begin, const RanAccIt end)
	{
		auto movingEnd = end;
		while (movingEnd != begin)
		{
			auto lastSwap = begin;
			auto previous = begin;

			for (auto next = (begin + 1); next != movingEnd; next++)
			{
				if (*previous > *next)
				{
					std::swap(*previous, *next);
					lastSwap = next;
				}

				previous = next;
			}

			movingEnd = lastSwap;
		}
	}
}