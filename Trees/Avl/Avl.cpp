#include"Avl.h"

#include<stack>

#include"Node.h"



namespace Avl
{
	inline auto findNode(Node* root, const int key) -> Node*
	{
		auto node = root;

		while (node != nullptr)
		{
			if (node->key == key)
				break;

			if (key < node->key)
			{
				node = node->leftChild.get();
			}
			else
			{
				node = node->rightChild.get();
			}
		}

		return node;
	}



	auto Avl::Add(const int key, int val) -> bool
	{
		if (Has(key))
			return false;

		std::unique_ptr<Node>* itNode = &root;
		if (*itNode == nullptr)
		{
			root = std::make_unique<Node>(key, val);
		}
		else
		{
			std::stack<std::unique_ptr<Node>*> parentsStack{};

			while (true)
			{
				if ((*itNode)->key == key)
					return false;

				parentsStack.emplace(itNode);


				if (key < (*itNode)->key)
				{
					if ((*itNode)->leftChild == nullptr)
					{
						auto newNode = std::make_unique<Node>(key, val);
						(*itNode)->AddLeftChild(newNode);
						parentsStack.emplace(&(*itNode)->leftChild);
						break;
					}
					else
					{
						itNode = &(*itNode)->leftChild;
					}
				}
				else
				{
					if ((*itNode)->rightChild == nullptr)
					{
						auto newNode = std::make_unique<Node>(key, val);
						(*itNode)->AddRightChild(newNode);
						parentsStack.emplace(&(*itNode)->rightChild);
						break;
					}
					else
					{
						itNode = &(*itNode)->rightChild;
					}
				}
			}

			auto& last = parentsStack.top();
			(*last)->balance = 0;
			parentsStack.pop();

			while (parentsStack.size() > 0)
			{
				auto& parent = parentsStack.top();

				if ((*parent)->leftChild == *last)
				{
					(*parent)->balance--;
				}
				else
				{
					(*parent)->balance++;
				}

				if ((*parent)->balance == 0)
				{
					// Adding this element actually rebalanced a node
					break;
				}

				if ((*parent)->balance == -2)
				{
					if ((*parent)->leftChild->balance < 0)
					{
						// Right rotation
						auto pivot = std::move((*parent)->leftChild);
						(*parent)->leftChild = std::move(pivot->rightChild);
						pivot.swap(*parent);
						(*parent)->rightChild = std::move(pivot);

						(*parent)->balance = 0;
						(*parent)->rightChild->balance = 0;

						break;
					}
					else
					{
						// Left-Right rotation
						auto pivot = std::move((*parent)->leftChild->rightChild);

						if (pivot->leftChild != nullptr)
							(*parent)->leftChild->rightChild = std::move(pivot->leftChild);


						pivot->leftChild = std::move((*parent)->leftChild);

						if (pivot->rightChild != nullptr)
							(*parent)->leftChild = std::move(pivot->rightChild);

						pivot->rightChild = std::move(*parent);
						(*parent) = std::move(pivot);

						if ((*parent)->balance == 0)
						{
							(*parent)->leftChild->balance = 0;
							(*parent)->rightChild->balance = 0;
						}
						else if ((*parent)->balance > 0)
						{
							(*parent)->balance = 0;
							(*parent)->leftChild->balance = -1;
							(*parent)->rightChild->balance = 0;
						}
						else
						{
							(*parent)->balance = 0;
							(*parent)->leftChild->balance = 0;
							(*parent)->rightChild->balance = 1;
						}

						break;
					}
				}

				if ((*parent)->balance == 2)
				{
					if ((*parent)->rightChild->balance > 0)
					{
						// Left rotation
						auto pivot = std::move((*parent)->rightChild);
						(*parent)->rightChild = std::move(pivot->leftChild);
						pivot.swap(*parent);
						(*parent)->leftChild = std::move(pivot);

						(*parent)->balance = 0;
						(*parent)->leftChild->balance = 0;

						break;
					}
					else
					{
						// Right-Left rotation
						auto pivot = std::move((*parent)->rightChild->leftChild);

						if (pivot->rightChild != nullptr)
							(*parent)->rightChild->leftChild = std::move(pivot->rightChild);

						pivot->rightChild = std::move((*parent)->rightChild);

						if (pivot->leftChild != nullptr)
							(*parent)->rightChild = std::move(pivot->leftChild);

						pivot->leftChild = std::move(*parent);
						(*parent) = std::move(pivot);

						if ((*parent)->balance == 0)
						{
							(*parent)->leftChild->balance = 0;
							(*parent)->rightChild->balance = 0;
						}
						else if ((*parent)->balance > 0)
						{
							(*parent)->balance = 0;
							(*parent)->leftChild->balance = -1;
							(*parent)->rightChild->balance = 0;
						}
						else
						{
							(*parent)->balance = 0;
							(*parent)->leftChild->balance = 0;
							(*parent)->rightChild->balance = 1;
						}

						break;
					}
				}

				last = parent;
				parentsStack.pop();
			}
		}

		return true;
	}



	auto Avl::Has(const int key) -> bool
	{
		auto node = findNode(root.get(), key);
		return (node != nullptr);
	}



	auto Avl::Get(const int key) -> std::optional<int>
	{
		auto node = findNode(root.get(), key);

		if (node != nullptr)
			return node->val;
		else
			return std::nullopt;
	}
}
