#include"../../catch.hpp"

#include<numeric>
#include<algorithm>
#include<random>
#include<chrono>

#include"Avl.h"
#include"Node.h"



namespace AvlTests
{
	// FIXME : validation function are adding way too many assertions in the
	//         logs. Consider refactoring to reduce amount?
	auto validateBalanceRecursive(const Avl::Node* node) -> int
	{
		if (node == nullptr)
			return 0;

		auto leftHeight = validateBalanceRecursive(node->leftChild.get());
		auto rightHeight = validateBalanceRecursive(node->rightChild.get());

		REQUIRE(node->balance == (rightHeight - leftHeight));

		return (leftHeight > rightHeight) ? leftHeight + 1 : rightHeight + 1;
	}

	void validateAvlBalance(const Avl::Avl& avl)
	{
		validateBalanceRecursive(avl.root.get());
	}

	TEST_CASE("Empty tree", "[Avl]")
	{
		Avl::Avl avl{};

		REQUIRE_FALSE(avl.Has(1));
		REQUIRE_FALSE(avl.Get(1));
	}

	TEST_CASE("Adding first element", "[Avl]")
	{

		Avl::Avl avl{};

		REQUIRE(avl.Add(1, 100));
		REQUIRE(avl.Has(1));
		REQUIRE(avl.Get(1));
		REQUIRE(avl.Get(1) == 100);

		REQUIRE_FALSE(avl.Has(2));
		REQUIRE_FALSE(avl.Get(2));

		REQUIRE(avl.root->balance == 0);
	}

	TEST_CASE("Adding root and left child", "[Avl]")
	{
		Avl::Avl avl;

		REQUIRE(avl.Add(1, 100));
		REQUIRE(avl.Add(-1, -100));

		REQUIRE(avl.Has(1));
		REQUIRE(avl.Get(1) == 100);
		REQUIRE(avl.Has(-1));
		REQUIRE(avl.Get(-1) == -100);

		REQUIRE(avl.root->balance == -1);
		REQUIRE(avl.root->leftChild->balance == 0);

		validateAvlBalance(avl);
	}

	TEST_CASE("Adding root and right child", "[Avl]")
	{
		Avl::Avl avl;

		REQUIRE(avl.Add(1, 100));
		REQUIRE(avl.Add(2, 200));

		REQUIRE(avl.Has(1));
		REQUIRE(avl.Get(1) == 100);
		REQUIRE(avl.Has(2));
		REQUIRE(avl.Get(2) == 200);

		REQUIRE(avl.root->balance == 1);
		REQUIRE(avl.root->rightChild->balance == 0);
	}

	TEST_CASE("Adding three decreasing elements to cause right rotation", "[Avl]")
	{
		Avl::Avl avl;

		REQUIRE(avl.Add(0, 0));
		REQUIRE(avl.Add(-1, -100));
		REQUIRE(avl.Add(-2, -200));

		REQUIRE(avl.Has(0));
		REQUIRE(avl.Get(0) == 0);
		REQUIRE(avl.Has(-1));
		REQUIRE(avl.Get(-1) == -100);
		REQUIRE(avl.Has(-2));
		REQUIRE(avl.Get(-2) == -200);

		REQUIRE(avl.root->key == -1);
		REQUIRE(avl.root->balance == 0);

		REQUIRE(avl.root->leftChild->key == -2);
		REQUIRE(avl.root->leftChild->balance == 0);

		REQUIRE(avl.root->rightChild->key == 0);
		REQUIRE(avl.root->rightChild->balance == 0);

		validateAvlBalance(avl);
	}

	TEST_CASE("Adding three increasing elements to cause left rotation", "[Avl]")
	{
		Avl::Avl avl;

		REQUIRE(avl.Add(0, 0));
		REQUIRE(avl.Add(1, 100));
		REQUIRE(avl.Add(2, 200));

		REQUIRE(avl.Has(0));
		REQUIRE(avl.Get(0) == 0);
		REQUIRE(avl.Has(1));
		REQUIRE(avl.Get(1) == 100);
		REQUIRE(avl.Has(2));
		REQUIRE(avl.Get(2) == 200);

		REQUIRE(avl.root->key == 1);
		REQUIRE(avl.root->balance == 0);

		REQUIRE(avl.root->leftChild->key == 0);
		REQUIRE(avl.root->leftChild->balance == 0);

		REQUIRE(avl.root->rightChild->key == 2);
		REQUIRE(avl.root->rightChild->balance == 0);
	}

	TEST_CASE("Adding three elements to cause a left-right rotation", "[Avl]")
	{
		Avl::Avl avl;

		REQUIRE(avl.Add(1, 100));
		REQUIRE(avl.Add(-1, -100));
		REQUIRE(avl.Add(0, 0));

		REQUIRE(avl.Has(1));
		REQUIRE(avl.Get(1) == 100);
		REQUIRE(avl.Has(-1));
		REQUIRE(avl.Get(-1) == -100);
		REQUIRE(avl.Has(0));
		REQUIRE(avl.Get(0) == 0);

		REQUIRE(avl.root->key == 0);
		REQUIRE(avl.root->balance == 0);

		REQUIRE(avl.root->leftChild->key == -1);
		REQUIRE(avl.root->leftChild->balance == 0);

		REQUIRE(avl.root->rightChild->key == 1);
		REQUIRE(avl.root->rightChild->balance == 0);

		validateAvlBalance(avl);
	}

	TEST_CASE("Adding three elements to cause a right-left rotation", "[Avl]")
	{
		Avl::Avl avl;

		REQUIRE(avl.Add(-1, -100));
		REQUIRE(avl.Add(1, 100));
		REQUIRE(avl.Add(0, 0));

		REQUIRE(avl.Has(-1));
		REQUIRE(avl.Get(-1) == -100);
		REQUIRE(avl.Has(1));
		REQUIRE(avl.Get(1) == 100);
		REQUIRE(avl.Has(0));
		REQUIRE(avl.Get(0) == 0);

		REQUIRE(avl.root->key == 0);
		REQUIRE(avl.root->balance == 0);

		REQUIRE(avl.root->leftChild->key == -1);
		REQUIRE(avl.root->leftChild->balance == 0);

		REQUIRE(avl.root->rightChild->key == 1);
		REQUIRE(avl.root->rightChild->balance == 0);
	}

	TEST_CASE("Additional rotations tests (on short avl)", "[Avl]")
	{
		// Test cases posted by Griffin on
		// https://stackoverflow.com/questions/3955680/how-to-check-if-my-avl-tree-implementation-is-correct
		Avl::Avl shortAvl;

		REQUIRE(shortAvl.Add(20, 20));
		REQUIRE(shortAvl.Add(4, 4));
		REQUIRE(shortAvl.Add(26, 26));
		REQUIRE(shortAvl.Add(3, 3));
		REQUIRE(shortAvl.Add(9, 9));

		REQUIRE(shortAvl.root->leftChild->leftChild->key == 3);
		REQUIRE(shortAvl.root->leftChild->leftChild->balance == 0);

		REQUIRE(shortAvl.root->leftChild->key == 4);
		REQUIRE(shortAvl.root->leftChild->balance == 0);

		REQUIRE(shortAvl.root->leftChild->rightChild->key == 9);
		REQUIRE(shortAvl.root->leftChild->rightChild->balance == 0);

		REQUIRE(shortAvl.root->key == 20);
		REQUIRE(shortAvl.root->balance == -1);

		REQUIRE(shortAvl.root->rightChild->key == 26);
		REQUIRE(shortAvl.root->rightChild->balance == 0);


		SECTION("Insert 15")
		{
			REQUIRE(shortAvl.Add(15, 15));

			REQUIRE(shortAvl.Has(3));
			REQUIRE(shortAvl.Get(3) == 3);

			REQUIRE(shortAvl.Has(4));
			REQUIRE(shortAvl.Get(4) == 4);

			REQUIRE(shortAvl.Has(9));
			REQUIRE(shortAvl.Get(9) == 9);

			REQUIRE(shortAvl.Has(15));
			REQUIRE(shortAvl.Get(15) == 15);

			REQUIRE(shortAvl.Has(20));
			REQUIRE(shortAvl.Get(20) == 20);

			REQUIRE(shortAvl.Has(26));
			REQUIRE(shortAvl.Get(26) == 26);

			REQUIRE(shortAvl.root->leftChild->leftChild->key == 3);
			REQUIRE(shortAvl.root->leftChild->leftChild->balance == 0);

			REQUIRE(shortAvl.root->leftChild->key == 4);
			REQUIRE(shortAvl.root->leftChild->balance == -1);

			REQUIRE(shortAvl.root->key == 9);
			REQUIRE(shortAvl.root->balance == 0);

			REQUIRE(shortAvl.root->rightChild->leftChild->key == 15);
			REQUIRE(shortAvl.root->rightChild->leftChild->balance == 0);

			REQUIRE(shortAvl.root->rightChild->key == 20);
			REQUIRE(shortAvl.root->rightChild->balance == 0);

			REQUIRE(shortAvl.root->rightChild->rightChild->key == 26);
			REQUIRE(shortAvl.root->rightChild->rightChild->balance == 0);

			validateAvlBalance(shortAvl);
		}

		SECTION("Insert 8")
		{
			REQUIRE(shortAvl.Add(8, 8));

			REQUIRE(shortAvl.Has(3));
			REQUIRE(shortAvl.Get(3) == 3);

			REQUIRE(shortAvl.Has(4));
			REQUIRE(shortAvl.Get(4) == 4);

			REQUIRE(shortAvl.Has(8));
			REQUIRE(shortAvl.Get(8) == 8);

			REQUIRE(shortAvl.Has(9));
			REQUIRE(shortAvl.Get(9) == 9);

			REQUIRE(shortAvl.Has(20));
			REQUIRE(shortAvl.Get(20) == 20);

			REQUIRE(shortAvl.Has(26));
			REQUIRE(shortAvl.Get(26) == 26);

			REQUIRE(shortAvl.root->leftChild->leftChild->key == 3);
			REQUIRE(shortAvl.root->leftChild->leftChild->balance == 0);

			REQUIRE(shortAvl.root->leftChild->key == 4);
			REQUIRE(shortAvl.root->leftChild->balance == 0);

			REQUIRE(shortAvl.root->leftChild->rightChild->key == 8);
			REQUIRE(shortAvl.root->leftChild->rightChild->balance == 0);

			REQUIRE(shortAvl.root->key == 9);
			REQUIRE(shortAvl.root->balance == 0);

			REQUIRE(shortAvl.root->rightChild->key == 20);
			REQUIRE(shortAvl.root->rightChild->balance == 1);

			REQUIRE(shortAvl.root->rightChild->rightChild->key == 26);
			REQUIRE(shortAvl.root->rightChild->rightChild->balance == 0);

			validateAvlBalance(shortAvl);
		}
	}

	TEST_CASE("Additional rotations tests (on long avl)", "[Avl]")
	{
		// Test cases posted by Griffin on
		// https://stackoverflow.com/questions/3955680/how-to-check-if-my-avl-tree-implementation-is-correct
		Avl::Avl longAvl;

		REQUIRE(longAvl.Add(20, 20));
		REQUIRE(longAvl.Add(4, 4));
		REQUIRE(longAvl.Add(26, 26));
		REQUIRE(longAvl.Add(3, 3));
		REQUIRE(longAvl.Add(9, 9));
		REQUIRE(longAvl.Add(21, 21));
		REQUIRE(longAvl.Add(30, 30));
		REQUIRE(longAvl.Add(2, 2));
		REQUIRE(longAvl.Add(7, 7));
		REQUIRE(longAvl.Add(11, 11));

		REQUIRE(longAvl.root->leftChild->leftChild->leftChild->key == 2);
		REQUIRE(longAvl.root->leftChild->leftChild->leftChild->balance == 0);

		REQUIRE(longAvl.root->leftChild->leftChild->key == 3);
		REQUIRE(longAvl.root->leftChild->leftChild->balance == -1);

		REQUIRE(longAvl.root->leftChild->key == 4);
		REQUIRE(longAvl.root->leftChild->balance == 0);

		REQUIRE(longAvl.root->leftChild->rightChild->leftChild->key == 7);
		REQUIRE(longAvl.root->leftChild->rightChild->leftChild->balance == 0);

		REQUIRE(longAvl.root->leftChild->rightChild->key == 9);
		REQUIRE(longAvl.root->leftChild->rightChild->balance == 0);

		REQUIRE(longAvl.root->leftChild->rightChild->rightChild->key == 11);
		REQUIRE(longAvl.root->leftChild->rightChild->rightChild->balance == 0);

		REQUIRE(longAvl.root->key == 20);
		REQUIRE(longAvl.root->balance == -1);

		REQUIRE(longAvl.root->rightChild->leftChild->key == 21);
		REQUIRE(longAvl.root->rightChild->leftChild->balance == 0);

		REQUIRE(longAvl.root->rightChild->key == 26);
		REQUIRE(longAvl.root->rightChild->balance == 0);

		REQUIRE(longAvl.root->rightChild->rightChild->key == 30);
		REQUIRE(longAvl.root->rightChild->rightChild->balance == 0);

		SECTION("Insert 15")
		{
			REQUIRE(longAvl.Add(15, 15));

			REQUIRE(longAvl.Has(2));
			REQUIRE(longAvl.Get(2) == 2);

			REQUIRE(longAvl.Has(3));
			REQUIRE(longAvl.Get(3) == 3);

			REQUIRE(longAvl.Has(4));
			REQUIRE(longAvl.Get(4) == 4);

			REQUIRE(longAvl.Has(7));
			REQUIRE(longAvl.Get(7) == 7);

			REQUIRE(longAvl.Has(9));
			REQUIRE(longAvl.Get(9) == 9);

			REQUIRE(longAvl.Has(11));
			REQUIRE(longAvl.Get(11) == 11);

			REQUIRE(longAvl.Has(15));
			REQUIRE(longAvl.Get(15) == 15);

			REQUIRE(longAvl.Has(20));
			REQUIRE(longAvl.Get(20) == 20);

			REQUIRE(longAvl.Has(21));
			REQUIRE(longAvl.Get(21) == 21);

			REQUIRE(longAvl.Has(26));
			REQUIRE(longAvl.Get(26) == 26);

			REQUIRE(longAvl.Has(30));
			REQUIRE(longAvl.Get(30) == 30);

			REQUIRE(longAvl.root->leftChild->leftChild->leftChild->key == 2);
			REQUIRE(longAvl.root->leftChild->leftChild->leftChild->balance == 0);

			REQUIRE(longAvl.root->leftChild->leftChild->key == 3);
			REQUIRE(longAvl.root->leftChild->leftChild->balance == -1);

			REQUIRE(longAvl.root->leftChild->key == 4);
			REQUIRE(longAvl.root->leftChild->balance == -1);

			REQUIRE(longAvl.root->leftChild->rightChild->key == 7);
			REQUIRE(longAvl.root->leftChild->rightChild->balance == 0);

			REQUIRE(longAvl.root->key == 9);
			REQUIRE(longAvl.root->balance == 0);

			REQUIRE(longAvl.root->rightChild->leftChild->key == 11);
			REQUIRE(longAvl.root->rightChild->leftChild->balance == 1);

			REQUIRE(longAvl.root->rightChild->leftChild->rightChild->key == 15);
			REQUIRE(longAvl.root->rightChild->leftChild->rightChild->balance == 0);

			REQUIRE(longAvl.root->rightChild->key == 20);
			REQUIRE(longAvl.root->rightChild->balance == 0);

			REQUIRE(longAvl.root->rightChild->rightChild->leftChild->key == 21);
			REQUIRE(longAvl.root->rightChild->rightChild->leftChild->balance == 0);

			REQUIRE(longAvl.root->rightChild->rightChild->key == 26);
			REQUIRE(longAvl.root->rightChild->rightChild->balance == 0);

			REQUIRE(longAvl.root->rightChild->rightChild->rightChild->key == 30);
			REQUIRE(longAvl.root->rightChild->rightChild->rightChild->balance == 0);

			validateAvlBalance(longAvl);
		}

		SECTION("Insert 8")
		{
			REQUIRE(longAvl.Add(8, 8));

			REQUIRE(longAvl.Has(2));
			REQUIRE(longAvl.Get(2) == 2);

			REQUIRE(longAvl.Has(3));
			REQUIRE(longAvl.Get(3) == 3);

			REQUIRE(longAvl.Has(4));
			REQUIRE(longAvl.Get(4) == 4);

			REQUIRE(longAvl.Has(7));
			REQUIRE(longAvl.Get(7) == 7);

			REQUIRE(longAvl.Has(8));
			REQUIRE(longAvl.Get(8) == 8);

			REQUIRE(longAvl.Has(9));
			REQUIRE(longAvl.Get(9) == 9);

			REQUIRE(longAvl.Has(11));
			REQUIRE(longAvl.Get(11) == 11);

			REQUIRE(longAvl.Has(20));
			REQUIRE(longAvl.Get(20) == 20);

			REQUIRE(longAvl.Has(21));
			REQUIRE(longAvl.Get(21) == 21);

			REQUIRE(longAvl.Has(26));
			REQUIRE(longAvl.Get(26) == 26);

			REQUIRE(longAvl.Has(30));
			REQUIRE(longAvl.Get(30) == 30);

			REQUIRE(longAvl.root->leftChild->leftChild->leftChild->key == 2);
			REQUIRE(longAvl.root->leftChild->leftChild->leftChild->balance == 0);

			REQUIRE(longAvl.root->leftChild->leftChild->key == 3);
			REQUIRE(longAvl.root->leftChild->leftChild->balance == -1);

			REQUIRE(longAvl.root->leftChild->key == 4);
			REQUIRE(longAvl.root->leftChild->balance == 0);

			REQUIRE(longAvl.root->leftChild->rightChild->key == 7);
			REQUIRE(longAvl.root->leftChild->rightChild->balance == 1);

			REQUIRE(longAvl.root->leftChild->rightChild->rightChild->key == 8);
			REQUIRE(longAvl.root->leftChild->rightChild->rightChild->balance == 0);

			REQUIRE(longAvl.root->key == 9);
			REQUIRE(longAvl.root->balance == 0);

			REQUIRE(longAvl.root->rightChild->leftChild->key == 11);
			REQUIRE(longAvl.root->rightChild->leftChild->balance == 0);

			REQUIRE(longAvl.root->rightChild->key == 20);
			REQUIRE(longAvl.root->rightChild->balance == 1);

			REQUIRE(longAvl.root->rightChild->rightChild->leftChild->key == 21);
			REQUIRE(longAvl.root->rightChild->rightChild->leftChild->balance == 0);

			REQUIRE(longAvl.root->rightChild->rightChild->key == 26);
			REQUIRE(longAvl.root->rightChild->rightChild->balance == 0);

			REQUIRE(longAvl.root->rightChild->rightChild->rightChild->key == 30);
			REQUIRE(longAvl.root->rightChild->rightChild->rightChild->balance == 0);

			validateAvlBalance(longAvl);
		}
	}

	TEST_CASE("Additional rotations tests (on short avl) - mirrored", "[Avl]")
	{
		// Test cases posted by Griffin on
		// https://stackoverflow.com/questions/3955680/how-to-check-if-my-avl-tree-implementation-is-correct
		//
		// Insertions changed so that the resulting tree and rotations are mirrored
		Avl::Avl shortAvl;

		REQUIRE(shortAvl.Add(20, 20));
		REQUIRE(shortAvl.Add(10, 10));
		REQUIRE(shortAvl.Add(30, 30));
		REQUIRE(shortAvl.Add(25, 25));
		REQUIRE(shortAvl.Add(35, 35));

		REQUIRE(shortAvl.root->leftChild->key == 10);
		REQUIRE(shortAvl.root->leftChild->balance == 0);

		REQUIRE(shortAvl.root->key == 20);
		REQUIRE(shortAvl.root->balance == 1);

		REQUIRE(shortAvl.root->rightChild->leftChild->key == 25);
		REQUIRE(shortAvl.root->rightChild->leftChild->balance == 0);

		REQUIRE(shortAvl.root->rightChild->key == 30);
		REQUIRE(shortAvl.root->rightChild->balance == 0);

		REQUIRE(shortAvl.root->rightChild->rightChild->key == 35);
		REQUIRE(shortAvl.root->rightChild->rightChild->balance == 0);


		SECTION("Insert 22")
		{
			REQUIRE(shortAvl.Add(22, 22));

			REQUIRE(shortAvl.Has(10));
			REQUIRE(shortAvl.Get(10) == 10);

			REQUIRE(shortAvl.Has(20));
			REQUIRE(shortAvl.Get(20) == 20);

			REQUIRE(shortAvl.Has(22));
			REQUIRE(shortAvl.Get(22) == 22);

			REQUIRE(shortAvl.Has(25));
			REQUIRE(shortAvl.Get(25) == 25);

			REQUIRE(shortAvl.Has(30));
			REQUIRE(shortAvl.Get(30) == 30);

			REQUIRE(shortAvl.Has(35));
			REQUIRE(shortAvl.Get(35) == 35);

			REQUIRE(shortAvl.root->leftChild->leftChild->key == 10);
			REQUIRE(shortAvl.root->leftChild->leftChild->balance == 0);

			REQUIRE(shortAvl.root->leftChild->rightChild->key == 22);
			REQUIRE(shortAvl.root->leftChild->rightChild->balance == 0);


			REQUIRE(shortAvl.root->leftChild->key == 20);
			REQUIRE(shortAvl.root->leftChild->balance == 0);

			REQUIRE(shortAvl.root->key == 25);
			REQUIRE(shortAvl.root->balance == 0);

			REQUIRE(shortAvl.root->rightChild->key == 30);
			REQUIRE(shortAvl.root->rightChild->balance == 1);

			REQUIRE(shortAvl.root->rightChild->rightChild->key == 35);
			REQUIRE(shortAvl.root->rightChild->rightChild->balance == 0);

			validateAvlBalance(shortAvl);
		}

		SECTION("Insert 27")
		{
			REQUIRE(shortAvl.Add(27, 27));

			REQUIRE(shortAvl.Has(10));
			REQUIRE(shortAvl.Get(10) == 10);

			REQUIRE(shortAvl.Has(20));
			REQUIRE(shortAvl.Get(20) == 20);

			REQUIRE(shortAvl.Has(25));
			REQUIRE(shortAvl.Get(25) == 25);

			REQUIRE(shortAvl.Has(27));
			REQUIRE(shortAvl.Get(27) == 27);

			REQUIRE(shortAvl.Has(30));
			REQUIRE(shortAvl.Get(30) == 30);

			REQUIRE(shortAvl.Has(35));
			REQUIRE(shortAvl.Get(35) == 35);

			REQUIRE(shortAvl.root->leftChild->leftChild->key == 10);
			REQUIRE(shortAvl.root->leftChild->leftChild->balance == 0);

			REQUIRE(shortAvl.root->leftChild->key == 20);
			REQUIRE(shortAvl.root->leftChild->balance == -1);

			REQUIRE(shortAvl.root->key == 25);
			REQUIRE(shortAvl.root->balance == 0);

			REQUIRE(shortAvl.root->rightChild->leftChild->key == 27);
			REQUIRE(shortAvl.root->rightChild->leftChild->balance == 0);

			REQUIRE(shortAvl.root->rightChild->key == 30);
			REQUIRE(shortAvl.root->rightChild->balance == 0);

			REQUIRE(shortAvl.root->rightChild->rightChild->key == 35);
			REQUIRE(shortAvl.root->rightChild->rightChild->balance == 0);

			validateAvlBalance(shortAvl);
		}
	}

	TEST_CASE("Additional rotations tests (on long avl) - mirrored", "[Avl]")
	{
		// Test cases posted by Griffin on
		// https://stackoverflow.com/questions/3955680/how-to-check-if-my-avl-tree-implementation-is-correct
		//
		// Insertions changed so that the resulting tree and rotations are mirrored
		Avl::Avl longAvl;

		REQUIRE(longAvl.Add(20, 20));
		REQUIRE(longAvl.Add(10, 10));
		REQUIRE(longAvl.Add(30, 30));
		REQUIRE(longAvl.Add(5, 5));
		REQUIRE(longAvl.Add(15, 15));
		REQUIRE(longAvl.Add(25, 25));
		REQUIRE(longAvl.Add(35, 35));
		REQUIRE(longAvl.Add(22, 22));
		REQUIRE(longAvl.Add(27, 27));
		REQUIRE(longAvl.Add(40, 40));

		REQUIRE(longAvl.root->leftChild->leftChild->key == 5);
		REQUIRE(longAvl.root->leftChild->leftChild->balance == 0);

		REQUIRE(longAvl.root->leftChild->key == 10);
		REQUIRE(longAvl.root->leftChild->balance == 0);

		REQUIRE(longAvl.root->leftChild->rightChild->key == 15);
		REQUIRE(longAvl.root->leftChild->rightChild->balance == 0);

		REQUIRE(longAvl.root->key == 20);
		REQUIRE(longAvl.root->balance == 1);

		REQUIRE(longAvl.root->rightChild->leftChild->leftChild->key == 22);
		REQUIRE(longAvl.root->rightChild->leftChild->leftChild->balance == 0);

		REQUIRE(longAvl.root->rightChild->leftChild->key == 25);
		REQUIRE(longAvl.root->rightChild->leftChild->balance == 0);

		REQUIRE(longAvl.root->rightChild->leftChild->rightChild->key == 27);
		REQUIRE(longAvl.root->rightChild->leftChild->rightChild->balance == 0);

		REQUIRE(longAvl.root->rightChild->key == 30);
		REQUIRE(longAvl.root->rightChild->balance == 0);

		REQUIRE(longAvl.root->rightChild->rightChild->key == 35);
		REQUIRE(longAvl.root->rightChild->rightChild->balance == 1);

		REQUIRE(longAvl.root->rightChild->rightChild->rightChild->key == 40);
		REQUIRE(longAvl.root->rightChild->rightChild->rightChild->balance == 0);

		SECTION("Insert 21")
		{
			REQUIRE(longAvl.Add(21, 21));

			REQUIRE(longAvl.Has(5));
			REQUIRE(longAvl.Get(5) == 5);

			REQUIRE(longAvl.Has(10));
			REQUIRE(longAvl.Get(10) == 10);

			REQUIRE(longAvl.Has(15));
			REQUIRE(longAvl.Get(15) == 15);

			REQUIRE(longAvl.Has(20));
			REQUIRE(longAvl.Get(20) == 20);

			REQUIRE(longAvl.Has(21));
			REQUIRE(longAvl.Get(21) == 21);

			REQUIRE(longAvl.Has(22));
			REQUIRE(longAvl.Get(22) == 22);

			REQUIRE(longAvl.Has(25));
			REQUIRE(longAvl.Get(25) == 25);

			REQUIRE(longAvl.Has(27));
			REQUIRE(longAvl.Get(27) == 27);

			REQUIRE(longAvl.Has(30));
			REQUIRE(longAvl.Get(30) == 30);

			REQUIRE(longAvl.Has(35));
			REQUIRE(longAvl.Get(35) == 35);

			REQUIRE(longAvl.Has(40));
			REQUIRE(longAvl.Get(40) == 40);

			REQUIRE(longAvl.root->leftChild->leftChild->leftChild->key == 5);
			REQUIRE(longAvl.root->leftChild->leftChild->leftChild->balance == 0);

			REQUIRE(longAvl.root->leftChild->leftChild->key == 10);
			REQUIRE(longAvl.root->leftChild->leftChild->balance == 0);

			REQUIRE(longAvl.root->leftChild->leftChild->rightChild->key == 15);
			REQUIRE(longAvl.root->leftChild->leftChild->rightChild->balance == 0);

			REQUIRE(longAvl.root->leftChild->key == 20);
			REQUIRE(longAvl.root->leftChild->balance == 0);

			REQUIRE(longAvl.root->leftChild->rightChild->leftChild->key == 21);
			REQUIRE(longAvl.root->leftChild->rightChild->leftChild->balance == 0);

			REQUIRE(longAvl.root->leftChild->rightChild->key == 22);
			REQUIRE(longAvl.root->leftChild->rightChild->balance == -1);

			REQUIRE(longAvl.root->key == 25);
			REQUIRE(longAvl.root->balance == 0);

			REQUIRE(longAvl.root->rightChild->leftChild->key == 27);
			REQUIRE(longAvl.root->rightChild->leftChild->balance == 0);

			REQUIRE(longAvl.root->rightChild->key == 30);
			REQUIRE(longAvl.root->rightChild->balance == 1);

			REQUIRE(longAvl.root->rightChild->rightChild->key == 35);
			REQUIRE(longAvl.root->rightChild->rightChild->balance == 1);

			REQUIRE(longAvl.root->rightChild->rightChild->rightChild->key == 40);
			REQUIRE(longAvl.root->rightChild->rightChild->rightChild->balance == 0);

			validateAvlBalance(longAvl);
		}

		SECTION("Insert 26")
		{
			REQUIRE(longAvl.Add(26, 26));

			REQUIRE(longAvl.Has(5));
			REQUIRE(longAvl.Get(5) == 5);

			REQUIRE(longAvl.Has(10));
			REQUIRE(longAvl.Get(10) == 10);

			REQUIRE(longAvl.Has(15));
			REQUIRE(longAvl.Get(15) == 15);

			REQUIRE(longAvl.Has(20));
			REQUIRE(longAvl.Get(20) == 20);

			REQUIRE(longAvl.Has(22));
			REQUIRE(longAvl.Get(22) == 22);

			REQUIRE(longAvl.Has(25));
			REQUIRE(longAvl.Get(25) == 25);

			REQUIRE(longAvl.Has(26));
			REQUIRE(longAvl.Get(26) == 26);

			REQUIRE(longAvl.Has(27));
			REQUIRE(longAvl.Get(27) == 27);

			REQUIRE(longAvl.Has(30));
			REQUIRE(longAvl.Get(30) == 30);

			REQUIRE(longAvl.Has(35));
			REQUIRE(longAvl.Get(35) == 35);

			REQUIRE(longAvl.Has(40));
			REQUIRE(longAvl.Get(40) == 40);

			REQUIRE(longAvl.root->leftChild->leftChild->leftChild->key == 5);
			REQUIRE(longAvl.root->leftChild->leftChild->leftChild->balance == 0);

			REQUIRE(longAvl.root->leftChild->leftChild->key == 10);
			REQUIRE(longAvl.root->leftChild->leftChild->balance == 0);

			REQUIRE(longAvl.root->leftChild->leftChild->rightChild->key == 15);
			REQUIRE(longAvl.root->leftChild->leftChild->rightChild->balance == 0);

			REQUIRE(longAvl.root->leftChild->key == 20);
			REQUIRE(longAvl.root->leftChild->balance == -1);

			REQUIRE(longAvl.root->leftChild->rightChild->key == 22);
			REQUIRE(longAvl.root->leftChild->rightChild->balance == 0);

			REQUIRE(longAvl.root->key == 25);
			REQUIRE(longAvl.root->balance == 0);

			REQUIRE(longAvl.root->rightChild->leftChild->leftChild->key == 26);
			REQUIRE(longAvl.root->rightChild->leftChild->leftChild->balance == 0);

			REQUIRE(longAvl.root->rightChild->leftChild->key == 27);
			REQUIRE(longAvl.root->rightChild->leftChild->balance == -1);

			REQUIRE(longAvl.root->rightChild->key == 30);
			REQUIRE(longAvl.root->rightChild->balance == 0);

			REQUIRE(longAvl.root->rightChild->rightChild->key == 35);
			REQUIRE(longAvl.root->rightChild->rightChild->balance == 1);

			REQUIRE(longAvl.root->rightChild->rightChild->rightChild->key == 40);
			REQUIRE(longAvl.root->rightChild->rightChild->rightChild->balance == 0);

			validateAvlBalance(longAvl);
		}
	}

	TEST_CASE("Inserting many elements in increasing and decreasing order", "[Avl]")
	{
		Avl::Avl avl{};

		SECTION("Increasing order")
		{
			for (int i = 0; i < 513; ++i)
			{
				avl.Add(i, i);
			}
			for (int i = 0; i < 513; ++i)
			{
				REQUIRE(avl.Has(i));
				auto getRes = avl.Get(i);
				REQUIRE(getRes == i);
			}

			validateAvlBalance(avl);
		}

		SECTION("Decreasing order")
		{
			for (int i = 512; i >= 0; --i)
			{
				avl.Add(i, i);
			}
			for (int i = 512; i >= 0; --i)
			{
				REQUIRE(avl.Has(i));
				auto getRes = avl.Get(i);
				REQUIRE(getRes == i);
			}

			validateAvlBalance(avl);
		}
	}

	TEST_CASE("Inserting elements in random order", "[Avl]")
	{
		// This test case is here to aid in finding errors, but since
		// insertion order is random, it's not guaranteed it will find unknown
		// bugs.
		Avl::Avl avl;

		std::vector<int> elements(2048);
		std::iota(std::begin(elements), std::end(elements), 0);

		auto seed = std::chrono::system_clock::now().time_since_epoch().count();
		shuffle(std::begin(elements), std::end(elements), std::default_random_engine(unsigned int(seed)));

		for (auto i : elements)
		{
			avl.Add(i, i);
		}

		validateAvlBalance(avl);

		for (auto i : elements)
		{
			REQUIRE(avl.Has(i));
			auto getRes = avl.Get(i);
			REQUIRE(getRes == i);
		}
	}

}
