#pragma once

#include<memory>
#include<optional>



namespace Avl
{
	struct Node;



	// Simple thread-unsafe AVL implementation
	class Avl
	{
	public:
		Avl() = default;
		Avl(const Avl&) = delete;
		Avl(Avl&&) = default;
		~Avl() = default;

		auto operator=(const Avl&)->Avl & = delete;
		auto operator=(Avl&&)->Avl & = default;

		auto Add(const int key, int val) -> bool;
		auto Has(const int key) -> bool;
		auto Get(const int key)->std::optional<int>;

		std::unique_ptr<Node> root;
	};
}