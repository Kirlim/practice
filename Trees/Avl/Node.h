#pragma once

#include<memory>



namespace Avl
{
	struct Node
	{
		const int key;
		int val;

		int balance = 0;
		std::unique_ptr<Node> leftChild;
		std::unique_ptr<Node> rightChild;

		Node(const int key, int val);
		~Node() = default;

		// These add methods trusts the imlementation and does
		// not verify if there's already a child added
		auto AddLeftChild(std::unique_ptr<Node>& node) -> void;
		auto AddRightChild(std::unique_ptr<Node>& node) -> void;
	};
}