#include"Node.h"



namespace Avl
{
	Node::Node(const int key, int val) :
		key(key), val(val)
	{
	}



	auto Node::AddLeftChild(std::unique_ptr<Node>& node) -> void
	{
		leftChild = std::move(node);
	}



	auto Node::AddRightChild(std::unique_ptr<Node>& node) -> void
	{
		rightChild = std::move(node);
	}
}
