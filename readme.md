# Practicing Code!

This repository is an educated dump of algorithms, data structures and other stuff that I study, practice or learn when programming with C++. I'm not a professional C++ programmer (my professional experience lies with C# and now Go), so it's not expected professional code quality here.

I hope that I can use this repository as reference somewhere in the future!

* [Sorting](sorting.md)