[TOC]

# Sorting Algorithms

## Bubble Sort


Bubble Sort is a very simple sorting algorithm (makes it easier to remember, too) and has poor performance in likely all cases. The behavior is very simple:

Given _n_ elements to sort, _n >= 2_:

* Start from the first element, _(0)_
* Compare it with the element _(1)_. If it's bigger, swap _(0)_ with _(1)_
* Repeat to _(1)_ with _(2)_, then _(2)_ with _(3)_, ...,  until _n_
* If no swaps occurred, the elements are sorted
* Otherwise, go to first step, 

### An execution example

Iteration 0

1. _(4, 1), 3, 2_
2. _1, (4, 3), 2_
3. _1, 3, (4, 2)_

Iteration 1

1. _(1, 3), 2, 4_
2. _1, (3, 2), 4_
3. _1, 2, (3, 4)_

Iteration 2

1. _(1, 2), 3, 4_
2. _1, (2, 3), 4_
3. _1, 2, (3, 4)_

At the end of iteration 2, now swaps have occurred, so the elements are sorted.

### Optimizations

In the first iteration, the highest value is guaranteed to be constantly swapped until the end of the elements. In the second iteration, the next highest element is guaranteed to be swapped until the second-to-last position, and so on. This means that we can optimize by reducing the number of elements to check for every iteration.

### Complexity

* Worst-case: O(n^2^) comparisons, O(n^2^) swaps
* Best-case: O(n) comparisons, O(1) swaps
* Average: O(n^2^) comparisons, O(n^2^) swaps

### Notes

Bubble Sort performance is too bad even among simple sorting algorithms. It should not be used unless the input data is guaranteed to be nearly sorted - even so, algorithms such as insertion sort may be preferred.

